# About
sfJwtJsonApiPlugin ("JsonApi") was developed at [JWT](http://jwt.com) to make it
  easier to create Ajax APIs for Symfony 1.4 projects.

This plugin provides both server- and client-side libraries that do all the
  heavy lifting, allowing the developer to focus on the business logic.

# Compatibility
## Symfony
This plugin is designed for Symfony 1.4.

## Javascript
This plugin includes a jQuery plugin designed to work with version 1.7.

Using the jQuery plugin is completely optional; you are more than welcome to
  build your own if you want to use other Javascript frameworks (please send us
  a pull request if you do!).

# Installation
Once the plugin is installed, the only other step is to execute
  `symfony plugin:publish-assets` to make the jQuery plugin accessible to web
  browsers.

# Usage
See USAGE.md

# Contributing
We welcome any and all suggestions, requests, (constructive) criticism, code,
  fixes, forks, success stories... in short, if you think it would improve the
  quality of JsonApi (or at least make us feel good about it), we'd love to see
  it.