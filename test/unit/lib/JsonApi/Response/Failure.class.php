<?php
/** Unit tests for JsonApi_Response_Failure and by extension,
 *    JsonApi_Response::factory().
 *
 * @author Phoenix Zerin <phoenix@todofixthis.com>
 *
 * @package sfJwtJsonApiPlugin
 * @subpackage test.lib.jsonapi.response
 */
class JsonApi_Response_FailureTest
  extends Test_Case_Unit_JsonApi_Response
{
  protected
    $_key,
    $_msg,
    $_errors;

  protected function _setUp(  )
  {
    parent::_setUp();

    $this->_key = 'foobar';
    $this->_msg = 'fizzbuzz';

    $this->_errors = array(
      $this->_key => $this->_msg
    );
  }

  public function testFailureFactory()
  {
    $response = $this->_failure($this->_errors);

    $this->assertInstanceOf(
      'JsonApi_Response_Failure',
      $response,
      'Expected well-formed failure response to have correct type.'
    );

    /** @noinspection PhpUndefinedFieldInspection */
    $errors = $response->errors;

    $this->assertArrayHasKey($this->_key, $errors,
      'Expected error message to have correct key in response detail.'
    );

    $this->assertEquals(
      $this->_msg,
      $errors[$this->_key],
      'Expected error message to be stored in the response detail.'
    );
  }

  public function testStatusValueMismatch()
  {
    $response = $this->_failure($this->_errors, self::STATUS_OK);

    $this->assertInstanceOf(
      'JsonApi_Response_Error',
      $response,
      'Expected error response when wrong status value returned in failure message.'
    );
  }

  public function testThrowException()
  {
    $response = $this->_failure($this->_errors);

    try
    {
      $response->throwException();
      $this->fail(
        'Expected JsonApi_Response_RethrownException when calling throwException().'
      );
    }
    catch( JsonApi_Response_RethrownException $e )
    {
    }
  }

  /** Malformed responses get converted to JsonApi_Response_Error.
   *
   * @see JsonApi_Response_ErrorTest
   */
}