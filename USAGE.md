# About This Guide
This guide documents the major functionality that JsonApi provides.  It can be
  considered a user reference guide with cookbook elements.

This guide does not contain API documentation, nor does it cover topics that
  might be of interest to developers who wish to make changes to JsonApi itself.

# Basic Concepts
The JsonApi works by accepting a standard HTTP request and responding with a
  specially-formatted JSON response.

The response is a JSON object that always contains two elements:  `status` and
  `detail`.

## Example Responses
### OK
This is an example of a response from a call to an API to increment the number
  of likes for an object.  The response indicates that the call was successful
  and includes the new like count so that the requester can update its UI
  accordingly.

    {
      "status": "ok",
      "detail": {
        "likes": 38
      }
    }

### FAIL
This is an example of a response from a call to the same API to increment the
  number of likes for an object.  The response indicates that the ID that was
  specified in the request does not reference a valid object.

    {
      "status": "fail",
      "detail": {
        "errors": {
          "object-id":  "Invalid."
        }
      }
    }

## Response Elements
### Status
The `status` element indicates whether the request succeeded or not.  It has
  two possible values:

- `"status": "ok"` indicates that the request succeeded (HTTP status `200 OK`).
- `"status": "fail"` indicates that something was wrong with the request (e.g.,
    a bad parameter value; HTTP status `400 Bad Request`).

### Detail
The contents of the `detail` element depend upon the `status` of the response:

- `"status": "ok"`:  `detail` contains any values specified by the server to
  send back to the requester, such as query results, a status value, etc.
- `"status": "fail"`:  `detail` contains a single sub-array called `errors`
  which contains the error messages generated by the server.

# Writing API Actions
Writing actions for JsonApi is very similar to writing any other Symfony action,
  with two minor differences:

  - The actions class should `extend JsonApi_Actions` instead of `sfActions`.
  - Every execution path should terminate with `return $this->success();` or
    `return $this->failure();`.

JsonApi also provides a number of methods to make it easier to validate incoming
  parameters.

Here is an example of a JsonApi module with an action to increment the number of
  likes for an object:

    # %sf_apps_dir%/frontend/modules/myapimodule/actions/actions.class.php

    <?php
    /** Powers the myapimodule module.
     */
    class myapimoduleActions extends JsonApi_Actions
    {
      /** Increment the number of likes for an object.
       *
       * @param sfWebRequest $request
       */
      public function executeLike( sfWebRequest $request )
      {
        /* Validate request method. */
        $this->requirePost();

        /* Validate request parameters. */
        $objectId = $this->getParam('object-id', array(
          new sfValidatorDoctrineChoice(array(
            'required'  => true,
            'model'     => 'MyObject'
          ))
        );

        /* Additional validation. */
        if( ! $this->getUser()->getAttribute('can-like-objects') )
        {
          $this->setFailure('user' => 'Not allowed.');
        }

        /* Check validation results. */
        if( $this->hasFailures() )
        {
          /* Failure response. */
          return $this->failure();
        }

        /* Fetch object to operate on. */
        $object = MyObjectTable::getInstance()->findOneById($objectId);

        /* Post-fetch validation. */
        if( $object->hasLike($this->getUser()->getAuthUser()) )
        {
          /* Failure response. */
          return $this->failure(array('object' => 'Already liked.'));
        }

        /* Apply like to object. */
        $object->addLike($this->getUser()->getAuthUser());
        $object->save();

        /* Success response. */
        return $this->success(array('likes' => $object->getLikeCount()));
      }
    }

Note that this action generates the responses described above in the **Basic
  Concepts** section.

## Validating the Request Method
For extra security, it is a good idea to require that all API requests are HTTP
  POST requests.

Although you can enforce this via Symfony's routing system, it is recommended
  that you not do this and instead use `$this->requirePost();`.

JsonApi includes a useful mechanism for debugging API calls that allows you to
  see a human-readable response in your browser which requires that an HTTP GET
  request is accepted under certain conditions (see **Debugging** below).

## Validating Parameters
After validating the request method, the next step for most API actions is to
  validate the request parameters.  JsonApi provides a method to make this
  very straightforward:  `$this->getParam()`.

`$this->getParam()` takes two arguments:  the name of the parameter and an array
  of `sfValidatorBase` instances used to validate the incoming value.  It
  returns the validated value, or `null` if the value did not pass validation.

Consider the following invocation of `$this->getParam()` from the previous
  example:

    $objectId = $this->getParam('object-id', array(
      new sfValidatorDoctrineChoice(array(
        'required'  => true,
        'model'     => 'MyObject'
      ))
    );

The above code will look for an incoming request parameter named `object-id`
  (equivalent to checking `$request->getParameter('object-id')`) and then
  cleans it using an `sfValidatorDoctrineChoice` validator to ensure that the
  incoming value references an existing `MyObject` record in the database.

If the incoming value is valid, `$objectId` will be set to that ID.  If not,
  `$objectId` will be set to null, and a failure message will be added to an
  internal list to be sent with a failure response (see **Failure** below).

## Failure
### After Validation
As noted above, every call to `$this->getParam()` will add a failure message to
  an internal list for each parameter that fails validation.

You can also add extra failure messages using `$this->setFailure()`.  In the
  example action, this code checks to see if the user is allowed to post likes:

    if( ! $this->getUser()->getAttribute('can-like-objects') )
    {
      $this->setFailure('user' => 'Not allowed.');
    }

If the user does not have the `can-like-objects` attribute, an additional
  failure message is generated.

Once all validation is complete, it is important to verify that it succeeded
  before continuing.  This can be achieved with the following code:

    if( $this->hasFailures() )
    {
      return $this->failure();
    }

`$this->hasFailures()` checks the internal list to see if there are any failure
  messages.  If any exist, `return $this->failure()` stops processing the action
  and sends a failure response back to the requester.  Any failure messages that
  were generated during validation are automatically included in the response.

It is important to `return` the result of `$this->failure()`, as the return
  value is used to instruct the view to handle the response differently
  depending on whether you are debugging the action (see the **Debugging**
  section below).

### Arbitrarily
In some cases, you might need to check for a failure case after you have started
  to process the action.

For example, in the example action, we need to check to see if the user has
  already liked the requested object, but this cannot be done until we have
  validated the incoming `object-id` value.

    /* Fetch object to operate on. */
    $object = MyObjectTable::getInstance()->findOneById($objectId);

    /* Post-fetch validation. */
    if( $object->hasLike($this->getUser()->getAuthUser()) )
    {
      return $this->failure(array('object' => 'Already liked.'));
    }

Note that you can pass an array to `$this->failure()`, which is a shortcut for
  calling `$this->addFailures()` (sets an array of failure messages) immediately
  before calling `$this->failure()`.

## Success
Once your action has finished processing, it is important to
  `return $this->success()` so that JsonApi can format the response correctly.

You can optionally pass an array of values to `$this->success()` to include them
  in the `detail` element of the response.

For example, the `like` action includes the updated like count in its response:

    return $this->success(array('likes' => $object->getLikeCount()));

# Invoking API Actions
JsonApi provides two libraries for invoking API actions:  a PHP stub for writing
  server-to-server communication libraries and a Javascript client for sending
  JsonApi requests from a browser.

## PHP
JsonApi provides a base class `JsonApi_Base` that you can use to create your API
  class.

Here is an example of a class used to communicate with the `like` controller
  from the above examples:

    # %sf_lib_dir%/jsonapi/MyApiModuleApi.class.php

    <?php
    /** Client API for interacting with the myapimodule module.
     */
    class MyApiModuleApi extends JsonApi_Base
    {
      /** Submits a request to like an object.
       *
       * @param int $objectId
       *
       * @return JsonApi_Response
       */
      static public function like( $objectId )
      {
        return self::_doApiCall(
          __CLASS__,
          '/myapimodule/like',
          array(
            'object-id' => $objectId
          ),
          'post'
        );
      }
    }

Invoking an API call is as simple as calling `self::_doApiCall()` in your custom
  API class.

`self::_doApiCall()` takes up to four parameters:

1. The name of the class.  This is used internally by `JsonApi_Base` to use the
  correct HTTP adapter for your class (often used for unit tests; see
  **Testing** below).

    Because JsonApi is designed to be compatible with PHP 5.2, you must specify
      `__CLASS__` as the value for this parameter.

2. The path to use for the request.  This value is not interpreted by Symfony's
  controller, so you cannot use e.g., route names here.

3. Array of request parameters to send with the request.

4. (optional) Request method.  This defaults to `'post'`.

### The Response
`self::_doApiCall()` returns a `JsonApi_Response` object, which provides a
  number of methods for interpreting the result of the request.

Here is an example of how to process the result of a request to the `like`
  action:

    # %sf_lib_dir%/user/MyApiUser.class.php

    <?php
    /** Invokes user-specific API methods.
     */
    class MyApiUser
    {
      /** Adds a like to an object.
       *
       * @param int $objectId
       *
       * @return int New like count for the object.
       */
      public function likeObject( $objectId )
      {
        $response = MyApiModuleApi::like($objectId);

        if( ! $response->isSuccess() )
        {
          $response->throwException();
        }

        return $response->likes;
      }
    }

Use the `isSuccess()` method to check to see if the response indicates a
  successful request (i.e., the `status` element is set to `'ok'`).

If the request failed, it is often easiest to call the response's
  `throwException()` method, which throws a `JsonApi_Response_Exception` with a
  (hopefully) useful message.

If the request was successful, you can pull any values out of the `detail`
  element in the response using the response's `__get()` magic method:

    return $response->likes;

## Javascript
JsonApi also comes packaged with a jQuery plugin that is designed to make it
  easy to write robust API clients for Javascript applications.

Here is an example of how to use the `jsonapi` jQuery plugin to implement a
  'like' button in Javascript:

    <img id="ajax-loader" src="/images/ajax-loader.gif" alt="Loading..." />
    <div id="like-errors"></div>
    <button id="like" data-object-id="42">Like</button>

    <script type="text/javascript">
    $(function(  ) {
      $('#like').jsonapi({
        /* Specify URL to requst. */
        'url':  '/myapimodule/like',

        /* Specify POST data sent with the request. */
        'data': function( button ) {
          return {
            'object-id': button.attr('data-object-id')
          };
        },

        /* Event handler triggered before ajax request fires. */
        'pre_execute': function(  ) {
          $('#ajax-loader').show();
          $('#like-errors').clear();
        },

        /* Event handler triggered after response is processed. */
        'post_execute': function(  ) {
          $('#ajax-loader').hide();
        },

        /* Event handler triggered when receiving a success response. */
        'success': function( response, button ) {
          button.text(response.likes + ' likes!');
        },

        /* Event handler triggered when receiving a failure response. */
        'failure': function( response, button ) {
          var ul = $(document.createElement('ul'));
          $.each(response.errors, function(key, error) {
            ul.append(
              $(document.createElement('li'))
                .text(key + ': ' + error)
            );
          });
          $('#like-errors').append(ul);
        },

        /* Event handler triggered when something goes wrong. */
        'error': function( error ) {
          $('#like-errors').text(error.message);
        }
      });
    });
    </script>

The `jsonapi` plugin has one required option `url` which specifies the URL for
  the ajax request.  The remaining options are not required and can be used as
  needed:

- `data` specifies the post data that are sent with the request.  You can
  specify a function here that will be executed each time the request fires.
- `pre_execute` is an event that is triggered before the ajax request is sent.
  In the example, it displays a loading graphic and clears any error messages
  from previous requests.
- `post_execute` is an event that is triggerd after the ajax response is
  received.  This event is always triggered, even if an error occurred.  In the
  example, it hides the loading graphic.
- `success` is an event that is triggered when a success response is received.
  In the example, it updates the text of the button with the new number of
  likes.
- `failure` is an event that is triggered when a failure response is received.
  In the example, it displays all failure messages in a div above the button.
- `error` is an event that is triggered when an error occurs, such as if the
  response is a `500 Internal Server Error` or is otherwise invalid.  In the
  example, it displays the error message in the div above the button.

There are many more options available.  The `jsonapi` jQuery plugin is
  well-documented; it is recommended that you review the plugin's
  `web/js/jquery.jsonapi.js` file for more information.

# Debugging
JsonApi provides a handy way to debug your API actions in your browser.  Simply
  invoke the API action in dev mode and pass any parameters via GET instead of
  POST.

For example, you could debug the `like` action by requesting
  `/frontend_dev.php/myapimodule/like/object-id/42` in your browser.

The resulting JSON object will be formatted with `print_r()` to make it easier
  for a human to read, and you will also have access to the Symfony web debug
  toolbar.

# Testing
## Unit Tests
Writing unit tests for classes that invoke API methods can be tricky, as you
  need to intercept the request and mock a response.

JsonApi provides a mock HTTP adapter that you can inject at runtime in your
  unit tests to mock API responses.

Note:  the following example assumes that you are using
  [sfJwtPhpUnitPlugin](https://github.com/JWT-OSS/sfJwtPhpUnitPlugin).

Suppose we wanted to write a unit test for a class named `MyApiUser`, which was
  a wrapper for `MyApiModuleApi` from the previous examples.  We would like to
  write a unit test for `MyApiUser::likeObject()` which implicitly calls
  `MyApiModuleApi::like()`.  Since we don't want to actually send an HTTP
  request during the test, we need to inject a mock HTTP client into the API
  class.

Here is what the test case looks like:

    # %sf_test_dir%/unit/lib/user/MyApiUser.test.php

    <?php
    /** Unit tests for MyApiUser.
     */
    class MyApiUserTest extends Test_Case_Unit
    {
      /** @var MyApiUser */
      protected $_user;

      /** @var JsonApi_Http_Client_Mock */
      protected $_adapter;

      protected function _setUp(  )
      {
        $this->_adapter = new JsonApi_Http_Client_Mock();
        JsonApi_Base::getInstance('MyApiMethodApi')
          ->setHttpClient($this->_adapter);

        $this->_user = new MyApiUser();
      }

      public function testLikeObject(  )
      {
        $objectId = 42;
        $likes    = 150;

        /* Seed the response from the server. */
        $this->_adapter->seedSuccess(
          '/myapimodule/like',
          array('object-id' => $objectId),
          array('likes' => $likes)
        );

        /* $this->_user->likeObject() implicitly invokes MyApiModuleApi::like().
         */
        $this->assertEquals($likes, $this->_user->likeObject($objectId),
          'Expected API to be invoked correctly.'
        );
      }
    }

In the `_setUp()` method, we inject the mock HTTP client into MyApiMethodApi:

    $this->_adapter = new JsonApi_Http_Client_Mock();
    JsonApi_Base::getInstance('MyApiMethodApi')
      ->setHttpClient($this->_adapter);

Instead of sending an HTTP request `JsonApi_Http_Client_Mock` will compare the
  request parameters against an array of seeded values and return any matching
  response it was seeded with.

When we begin our test, we start by seeding the mock adapter with a success
  response:

    $this->_adapter->seedSuccess(
      '/myapimodule/like',
      array('object-id' => $objectId),
      array('likes' => $likes)
    );

Notice that the first two arguments are the path and parameters for the request,
  similar to the second and third parameters to `JsonApi_Base::_doApiCall()`
  (see **Invoking API Actions** above).  This tells the mock adapter which
  request to look out for.

The third parameter is the `detail` element from the simulated response.

You can alternatively use `$this->_adapter->seedFailure()` to seed a failure
  response.

Once the adapter has been seeded, you can then invoke the method that implicitly
  calls the API:

    $this->assertEquals($likes, $this->_user->likeObject($objectId),
      'Expected API to be invoked correctly.'
    );

What you are testing for here is to make sure that `$this->_user->likeObject()`
  invokes the API and processes the response correctly.

## Functional Tests
To write functional tests for your JsonApi-powered actions, you can leverage the
  `JsonApiResponse` plugin for `Test_Browser`.

Note:  the following example assumes that you are using
  [sfJwtPhpUnitPlugin](https://github.com/JWT-OSS/sfJwtPhpUnitPlugin).

Here is an example of a functional test for the `like` action from previous
  examples:

    # %sf_test_dir%/functional/frontend/myapimodule/like.test.php

    <?php
    /** Functional tests for /myapimodule/like.
     */
    class frontend_myapimodule_likeTest extends Test_Case_Functional
    {
      protected
        $_path = '/myapimodule/like',
        $_params;

      /** @var MyObject */
      $_object;

      protected function _setUp(  )
      {
        /* Enable JsonApiResponse plugin. */
        $this->usePlugin('JsonApiResponse');

        $this->loadFixture('MyObject/likeable.php');
        $this->_object = $this->getFixtureVar('LikeableObject');

        $this->_params = array(
          'object-id' => $this->_object->getId()
        );

        /* Sign in as a user with liking privileges. */
        $this->loadFixture('sfGuardUser/customer.php');
        $this->signin($this->getFixtureVar('CustomerUser'));
      }

      public function testLikeableObject(  )
      {
        $this->_browser->_post($this->_path, $this->_params);
        $this->assertStatusCode(200);

        $response = $this->_browser->getJsonApiResponse();

        $this->assertTrue($response->isSuccess(),
          'Expected success response.'
        );

        $this->assertEquals(1, $response->likes,
          'Expected correct number of resulting likes for object.'
        );
      }
    }

Because `JsonApiResponse` is not a standard browser plugin, you must enable it
  before you can use it:

    $this->usePlugin('JsonApiResponse');

In your tests, you can then call `$this->_browser->post()` as usual.  When you
  want to inspect the response, however, you first invoke the `JsonApiResponse`
  browser plugin:

    $response = $this->_browser->getJsonApiResponse();

`$response` now references an encapsulated `JsonApi_Response` object that you
  can interact with just like if you had invoked the API directly in PHP (see
  **Invoking API Actions** above).
